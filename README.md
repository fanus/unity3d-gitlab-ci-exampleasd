## CONTINUE WITH THIS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

See repo this was forked from for original instructions.

### Steps

1. log into AWS EC2 instance
2. install docker (`apt-get install docker.io`)
3. bash into the docker container
```
UNITY_VERSION=2018.3.12f1
docker run -it --rm \
-e "UNITY_USERNAME=fanus.link@gmail.com" \
-e "UNITY_PASSWORD=YOURPASSWORDHERE" \
-e "TEST_PLATFORM=linux" \
-e "WORKDIR=/root/project" \
-v "$(pwd):/root/project" \
gableroux/unity3d:$UNITY_VERSION \
bash
```
4. execute this in the docker container
```
xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' \
/opt/Unity/Editor/Unity \
-logFile \
-batchmode \
-username "$UNITY_USERNAME" -password "$UNITY_PASSWORD"
```
5. wait for xml to appear (see example below) and copy into local unity3d.alf file.
```
<?xml version="1.0" encoding="UTF-8"?><root><SystemInfo><IsoCode>en</IsoCode>...</root>
```
6. upload that file here: https://license.unity3d.com/manual
7. after activation, copy contents of downloaded file into a gitlab runner environment varialbe.
`gitlab.com -> Project Page -> Settings -> CI / CD -> Variables -> UNITY_LICENSE_CONTENT = <PASTE CONTENS HERE>`
8. exit docker container to return to aws instance bash
9. install gitlab-runner from here: https://docs.gitlab.com/runner/install/linux-repository.html (**do not use** `apt-get install gitlab-runner`)
10. register the runner using the registration token from `gitlab.com -> Project Page -> Settings -> CI / CD -> Runners -> Set up a specific Runner manually`
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "<PASTE TOKEN HERE>" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false"
```
11. run the pipeline `gitlab.com -> Project Page -> CI / CD -> Pipelines -> Run Pipeline`